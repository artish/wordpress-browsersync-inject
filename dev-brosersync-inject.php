<?php
/**
 * Plugin Name: BrowserSync Inject
 * Plugin URI: https://bitbucket.org/artish/wordpress-browsersync-inject
 * Description: Injects the browsersync script into the wordpress footer
 * Author: Florian Schrödl
 * Author URI: http://florianschroedl.com
 * Version: 0.1.0
 */

function dev_browsersync_inject() {
  ?>
  <script type='text/javascript' id="__bs_script__">//<![CDATA[
      document.write("<script async src='//HOST:3000/browser-sync/browser-sync-client.1.9.1.js'><\/script>".replace(/HOST/g, location.hostname).replace(/PORT/g, location.port));
  //]]></script>
  <?php
}
add_action('wp_footer', 'dev_browsersync_inject');